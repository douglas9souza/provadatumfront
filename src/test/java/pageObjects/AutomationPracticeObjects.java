package pageObjects;

import commons.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AutomationPracticeObjects extends BasePage {

    public AutomationPracticeObjects(WebDriver driver) { super(driver); }

    public WebElement getElementById(String id) {
        return driver.findElement(By.id(id));
    }

    public WebElement getElementByXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public WebElement getElementByName(String name) { return driver.findElement(By.name(name)); }

    public WebElement getElementByLinkText(String link) { return driver.findElement(By.linkText(link));}

    public WebElement getElementByCssSelector(String css) { return driver.findElement(By.cssSelector(css)); }

}