package automationPractice;

import commons.WebDriverUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pageBase.AutomationPracticePage;

public class AutomationPracticeTest {

    public static WebDriver driver = WebDriverUtil.openBrowser();
    AutomationPracticePage page = new AutomationPracticePage(driver);

    @AfterClass
    public static void quitBrowser() {
        driver.quit();
    }

    @Before
    public void signIn() {
        this.page.clickLinkSignIn();
        this.page.setFieldById("email", "datumqatest@soprock.com");
        this.page.setFieldById("passwd", "datum2021");
        this.page.clickButtonSignIn();
        this.page.validateSignIn();
    }

    @After
    public void signOut() {
        this.page.clickLinkSignOut();
        this.page.validateSignOut();
    }

    @Test
    public void testBuyItemsPaymentBankWire() {
        this.page.clickIconHome();
        this.page.clickImgBlouse();
        this.page.clickBlouseAddToCart();
        this.page.clickContinueShopping();
        this.page.clickReturnHome();
        this.page.clickImgPrintedDress();
        this.page.clickPrintedDressAddToCart();
        this.page.clickContinueShopping();
        this.page.clickReturnHome();
        this.page.clickLinkCart();
        this.page.clickProceedToCheckout1();
        this.page.clickProceedToCheckout2();
        this.page.clickCheckboxTermsOfService();
        this.page.clickProceedToCheckout3();
        this.page.selectPaymentBankWire();
        this.page.clickButtonConfirmMyOrder();
        this.page.validateOrderConfirmationBankWire();
    }

    @Test
    public void testBuyItemsPaymentCheck() {
        this.page.clickIconHome();
        this.page.clickImgBlouse();
        this.page.clickBlouseAddToCart();
        this.page.clickContinueShopping();
        this.page.clickReturnHome();
        this.page.clickImgPrintedDress();
        this.page.clickPrintedDressAddToCart();
        this.page.clickContinueShopping();
        this.page.clickReturnHome();
        this.page.clickLinkCart();
        this.page.clickProceedToCheckout1();
        this.page.clickProceedToCheckout2();
        this.page.clickCheckboxTermsOfService();
        this.page.clickProceedToCheckout3();
        this.page.selectPaymentCheck();
        this.page.clickButtonConfirmMyOrder();
        this.page.validateOrderConfirmationCheck();
    }

}