package pageBase;

import commons.BasePage;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import pageObjects.AutomationPracticeObjects;

import java.util.concurrent.TimeUnit;

public class AutomationPracticePage extends BasePage {

    public AutomationPracticePage(WebDriver driver) { super(driver); }

    AutomationPracticeObjects pageObjects = new AutomationPracticeObjects(driver);

    public void clickLinkSignIn() {
        pageObjects.getElementByLinkText("Sign in").click();
    }

    public void setFieldById(String element, String value) {
        pageObjects.getElementById(element).sendKeys(value);
    }

    public void clickButtonSignIn() {
        pageObjects.getElementById("SubmitLogin").click();
    }

    public void validateSignIn() {
        String text = pageObjects.getElementByXpath("//*[@id=\"columns\"]/div[1]/span[2]").getText();
        Assert.assertEquals("My account", text);
    }

    public void clickIconHome() {
        pageObjects.getElementByXpath("//*[@id=\"columns\"]/div[1]/a").click();
    }

    public void clickImgBlouse() {
        pageObjects.getElementByXpath("//*[@id=\"homefeatured\"]/li[2]/div/div[1]/div/a[1]").click();
    }
    public void clickBlouseAddToCart() {
        pageObjects.getElementByName("Submit").click();
    }

    public void clickContinueShopping() {
        waitTo(1);
        pageObjects.getElementByCssSelector("span.continue.btn.btn-default.button.exclusive-medium").click();
    }

    public void clickReturnHome() {
        pageObjects.getElementByCssSelector("i.icon-home").click();
    }

    public void clickImgPrintedDress() {
        pageObjects.getElementByXpath("//*[@id=\"homefeatured\"]/li[3]/div/div[1]/div/a[1]/img").click();
    }

    public void clickPrintedDressAddToCart() {
        pageObjects.getElementByName("Submit").click();
    }

    public void clickLinkCart() {
        pageObjects.getElementByXpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a").click();
    }

    public void clickProceedToCheckout1() {
        pageObjects.getElementByLinkText("Proceed to checkout").click();
    }

    public void clickProceedToCheckout2() {
        pageObjects.getElementByName("processAddress").click();
    }

    public void clickProceedToCheckout3() {
        pageObjects.getElementByName("processCarrier").click();
    }

    public void clickCheckboxTermsOfService() {
        pageObjects.getElementById("cgv").click();
    }

    public void selectPaymentBankWire() {
        pageObjects.getElementByXpath("//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a").click();
    }

    public void selectPaymentCheck() {
        pageObjects.getElementByXpath("//*[@id=\"HOOK_PAYMENT\"]/div[2]/div/p/a").click();
    }

    public void clickButtonConfirmMyOrder() {
        pageObjects.getElementByXpath("//*[@id=\"cart_navigation\"]/button").click();
    }

    public void validateOrderConfirmationBankWire() {
        String text = pageObjects.getElementByXpath("//*[@id=\"center_column\"]/h1").getText();
        Assert.assertEquals("ORDER CONFIRMATION", text);
    }

    public void validateOrderConfirmationCheck() {
        String text = pageObjects.getElementByXpath("//*[@id=\"center_column\"]/p[1]").getText();
        Assert.assertEquals("Your order on My Store is complete.", text);
    }

    public void clickLinkSignOut() {
        pageObjects.getElementByLinkText("Sign out").click();
    }

    public void validateSignOut() {
        String text = pageObjects.getElementByXpath("//*[@id=\"center_column\"]/h1").getText();
        Assert.assertEquals("AUTHENTICATION", text);
    }


}