<h1 align="center">Desafio de Automação de testes Datum (FrontEnd)</h1>

<h2> Sobre o projeto </h2>
<p>Projeto de automação de testes FrontEnd, baseado em <b>Maven</b>, com o framework <b>Selenium WebDriver</b> utilizando a linguagem de programação <b>Java</b>. Foi utilizado o browser Google Chrome.</p>
<p>O projeto foi desenvolvido utilizando a IDE <b>IntelliJ</b>.</p>

<p><b>Versão do Java: 1.8.241</b></p>
<p><b>Versão do Maven: 3.6.3</b></p>


